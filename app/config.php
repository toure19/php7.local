<?php
/**
 * Created by PhpStorm.
 * User: toure19_notebook
 * Date: 11/12/2018
 * Time: 8:51
 */

return [
    'database'=>[
        'name' => 'cursophp7',
        'username' => 'cursophp7',
        'password' => 'php',
        'connection' => 'mysql:host=php7.local',
        'options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => true
        ]
    ]
];

?>