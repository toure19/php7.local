<?php

require_once 'entity/ImagenGaleria.php';
require_once 'database/QueryBuilder.php';

/**
 * @var ImagenGaleria[] $imagenes
 */
$imagenes = [
    new ImagenGaleria('1.jpg','Descripcion imagen 1',1,1,1),
    new ImagenGaleria('2.jpg','Descripcion imagen 2',2,2,2),
    new ImagenGaleria('3.jpg','Descripcion imagen 3',3,3,3),
    new ImagenGaleria('4.jpg','Descripcion imagen 4',4,4,4),
    new ImagenGaleria('5.jpg','Descripcion imagen 5',5,5,5),
    new ImagenGaleria('6.jpg','Descripcion imagen 6',6,6,6),
    new ImagenGaleria('7.jpg','Descripcion imagen 7',7,7,7),
    new ImagenGaleria('8.jpg','Descripcion imagen 8',8,8,8),
    new ImagenGaleria('9.jpg','Descripcion imagen 9',9,9,9),
    new ImagenGaleria('10.jpg','Descripcion imagen 10',10,10,10),
    new ImagenGaleria('11.jpg','Descripcion imagen 11',11,11,11),
    new ImagenGaleria('12.jpg','Descripcion imagen 12',12,12,12)
];


require 'utils/utils.php';
require 'views/index.view.php';