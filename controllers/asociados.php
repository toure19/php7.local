<?php
/**
 * Created by PhpStorm.
 * User: toure19_notebook
 * Date: 18/12/2018
 * Time: 8:23
 */


require_once __DIR__.'/../utils/utils.php';
require_once __DIR__.'/../utils/File.php';
require_once __DIR__.'/../entity/Asociado.php';
require_once __DIR__.'/../database/Connection.php';
require_once __DIR__.'/../database/QueryBuilder.php';
require_once __DIR__.'/../core/App.php';
require_once __DIR__.'/../exception/FileException.php';
require_once __DIR__.'/../exception/QueryException.php';
require_once __DIR__.'/../repository/AsociadoRepository.php';


$errores = [];
$descripcion = '';
$nombre = '';
$mensaje = '';
try {
    $config = require_once __DIR__."/../app/config.php";
    App::bind('config', $config);
    $asociadoRepository = new AsociadoRepository();

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $nombre = trim(htmlspecialchars($_POST['nombre']));
        $descripcion = trim(htmlspecialchars($_POST['descripcion']));

        $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
        $imagen = new File('imagen', $tiposAceptados);
        $imagen->saveUploadFile(Asociado::RUTA_IMAGENES);

        $asociado = new Asociado($nombre,$imagen->getFileName(), $descripcion);
        $asociadoRepository->save($asociado);
        $mensaje = "Se ha guardado la imagen";
        $descripcion = "";
    }
    $asociados = $asociadoRepository->findAll();
} catch (QueryException $exception) {
    throw new QueryException("Error de BD");
} catch (FileException $exception) {
    throw new FileException("Error al inserar fichero");
}

require __DIR__.'/../views/asociados.view.php';