<?php
/**
 * Created by PhpStorm.
 * User: toure19_notebook
 * Date: 06/12/2018
 * Time: 18:31
 */

class FileException extends Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}