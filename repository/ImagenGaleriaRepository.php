<?php
/**
 * Created by PhpStorm.
 * User: toure19_notebook
 * Date: 13/12/2018
 * Time: 12:13
 */

class ImagenGaleriaRepository extends QueryBuilder
{
    public function __construct(string $table = 'imagenes', string $classEntity = 'ImagenGaleria')
    {
        parent::__construct($table, $classEntity);
    }
}