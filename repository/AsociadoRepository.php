<?php
/**
 * Created by PhpStorm.
 * User: toure19_notebook
 * Date: 18/12/2018
 * Time: 8:30
 */

class AsociadoRepository extends QueryBuilder
{
    public function __construct(string $table = 'asociados', string $classEntity = 'Asociado')
    {
        parent::__construct($table, $classEntity);
    }
}